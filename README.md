Un script util care vă permite să descărcați și să instalați Thorium Browser pe sistemele Fedora. Acest script bash simplu detectează automat arhitectura procesorului și selectează versiunea corespunzătoare a Thorium Browser pentru descărcare și instalare. Iată o scurtă descriere a modului în care funcționează scriptul:

1. **Detectarea Automată a Arhitecturii Procesorului**: Scriptul începe prin a folosi comanda `uname -m` pentru a obține tipul de arhitectură a procesorului (de exemplu, x86_64 sau i686).

2. **Determinarea Sufixului Corespunzător Arhitecturii Procesorului**: Folosind comanda `case`, scriptul selectează un sufix specific pentru arhitectura detectată, care va fi utilizat pentru a descărca versiunea corectă a Thorium Browser.

3. **Obținerea Celei Mai Recente Versiuni de pe GitHub**: Scriptul folosește `curl` pentru a accesa API-ul GitHub și pentru a obține cea mai recentă versiune a Thorium Browser, utilizând `jq` pentru a analiza răspunsul JSON și pentru a extrage numărul de versiune.

4. **Construirea URL-ului de Descărcare pentru Versiunea Cea Mai Recentă și Sufixul Specificat de Arhitectură**: Utilizând informațiile obținute anterior, scriptul construiește un URL de descărcare pentru versiunea corespunzătoare a Thorium Browser.

5. **Descărcarea Thorium Browser**: Utilizând comanda `wget`, scriptul descarcă pachetul RPM al Thorium Browser de pe URL-ul construit anterior.

6. **Instalarea Thorium Browser**: După ce a fost descărcat, scriptul folosește `dnf` pentru a instala Thorium Browser.

7. **Ștergerea Pachetului RPM După Instalare**: În final, scriptul șterge pachetul RPM descărcat, pentru a curăța spațiul de lucru.

Acest script vă permite să instalați Thorium Browser pe sistemele bazate pe Fedora, fără a fi necesară descărcarea manuală sau configurarea suplimentară.