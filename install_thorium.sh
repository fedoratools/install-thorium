#!/bin/bash

# Detectarea automată a arhitecturii procesorului
architecture=$(uname -m)

# Determinarea sufixului corespunzător arhitecturii procesorului
case $architecture in
    x86_64)
        suffix="AVX2.rpm"
        ;;
    i686|i386)
        suffix="SSE3.rpm"
        ;;
    *)
        echo "Arhitectura procesorului nu este suportată."
        exit 1
        ;;
esac

# Obținerea celei mai recente versiuni de pe GitHub
latest_version=$(curl -s https://api.github.com/repos/Alex313031/thorium/releases/latest | jq -r '.tag_name')

if [[ -z "$latest_version" ]]; then
    echo "Nu s-a putut obține versiunea cea mai recentă a Thorium Browser de pe GitHub."
    exit 1
fi

# Construirea URL-ului de descărcare pentru versiunea cea mai recentă și sufixul specificat de arhitectură
download_url=$(curl -s https://api.github.com/repos/Alex313031/thorium/releases/latest | jq -r --arg suffix "$suffix" '.assets[] | select(.name | endswith($suffix)) | .browser_download_url')

# Descărcarea Thorium Browser
wget "$download_url" -O thorium-browser.rpm

if [[ ! -f "thorium-browser.rpm" ]]; then
    echo "Eroare la descărcare. Nu s-a putut găsi pachetul RPM pentru Thorium Browser."
    exit 1
fi

# Instalarea Thorium Browser
sudo dnf install thorium-browser.rpm

# Ștergerea pachetului RPM după instalare
rm thorium-browser.rpm

